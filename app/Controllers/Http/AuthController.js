"use strict";

const User = use("App/Models/User");

class AuthController {
  async redirectToGoogle({ ally }) {
    await ally.driver("google").redirect();
  }

  async handleGoogleCallback({ ally, auth, response }) {
    try {
      // Store the user data gotten from provider
      const userData = await ally.driver("google").getUser();

      // Try to see if the email exist
      const authUser = await User.findBy("email", userData.getEmail());

      if (!(authUser === null)) {
        await auth.loginViaId(authUser.id);
        return response.redirect("/");
      }

      let fullName = userData.getName();
      let firstName = fullName.split(" ").slice(0, -1).join(" ");
      let lastName = fullName.split(" ").slice(-1).join(" ");

      const user = new User();
      user.first_name = firstName;
      user.last_name = lastName;
      user.email = userData.getEmail();
      await user.save();

      await auth.loginViaId(user.id);
      return response.redirect("/");
    } catch (e) {
      console.log(e);
      response.redirect("/");
    }
  }

  async logout({ auth, response }) {
    await auth.logout();
    response.redirect("/");
  }
}

module.exports = AuthController;
